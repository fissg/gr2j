# JSON Schema


## Oid

Represents a [Git Object ID](https://www.pygit2.org/oid.html).

| Field Name | Type   | Description |
|------------|--------|-------------|
| `full`       | string | The full 40-character SHA1 hexdigest of the Object ID. |
| `abbrev`     | string | The first 8 characters of `full`. Warning: This is an ambiguous abbreviation. There may be collisions! |
| `h`          | string | The first 2 characters of `full`.  (head)|
| `t`          | string | All characters of `full` except the first two. (tail) |

## Email

Represents an Email Address.

| Field Name | Type   | Description |
|------------|--------|-------------|
| `orig`       | string | The full unaltered email address. |
| `lower`      | string | Lowercase version of `orig`. |
| `local_part` | string | The [Local-part](https://en.wikipedia.org/wiki/Email_address#Local-part) of `lower`. |
| `domain`     | string | The [Domain](https://en.wikipedia.org/wiki/Email_address#Domain) of `lower`.|
| `md5`        | string | The MD5 hexdigest of `lower`. This can be useful for services like [Gravatar](https://en.wikipedia.org/wiki/Gravatar).  |

## Date

Represents a [datetime Object](https://docs.python.org/3/library/datetime.html#datetime-objects).

| Field Name | Type   | Description |
|------------|--------|-------------|
| `custom`     | string | The date formatted according to the `--dateformat` command line argument. |
| `rfc2822`    | string | See [email.utils.format_datetime](https://docs.python.org/3/library/email.utils.html#email.utils.format_datetime). |
| `year`       | number | See [datetime.datetime.year](https://docs.python.org/3/library/datetime.html#datetime.datetime.year). |
| `month`      | number | See [datetime.datetime.month](https://docs.python.org/3/library/datetime.html#datetime.datetime.month). |
| `day`        | number | See [datetime.datetime.day](https://docs.python.org/3/library/datetime.html#datetime.datetime.day). |
| `hour`       | number | See [datetime.datetime.hour](https://docs.python.org/3/library/datetime.html#datetime.datetime.hour). |
| `minute`     | number | See [datetime.datetime.minute](https://docs.python.org/3/library/datetime.html#datetime.datetime.minute). |
| `second`     | number | See [datetime.datetime.second](https://docs.python.org/3/library/datetime.html#datetime.datetime.second). |
| `microsecond`| number | See [datetime.datetime.microsecond](https://docs.python.org/3/library/datetime.html#datetime.datetime.microsecond). |
| `tzoffset_in_minutes` | number | The timezone offset in minutes. |
| `tzname`     | string | See [datetime.datetime.tzname](https://docs.python.org/3/library/datetime.html#datetime.datetime.tzname). |
| `posix_timestamp` | number | The posix timestamp. |
| `weekday`    | number | See [datetime.datetime.weekday](https://docs.python.org/3/library/datetime.html#datetime.datetime.weekday). |
| `isoweekday`| number | See [datetime.datetime.isoweekday](https://docs.python.org/3/library/datetime.html#datetime.datetime.isoweekday). |
| `isocalendar`| \[number\] | See [datetime.datetime.isocalendar](https://docs.python.org/3/library/datetime.html#datetime.datetime.isocalendar). |
| `iso8601`| string | See [datetime.datetime.isoformat](https://docs.python.org/3/library/datetime.html#datetime.datetime.isoformat). |
| `ctime`| string | See [datetime.datetime.ctime](https://docs.python.org/3/library/datetime.html#datetime.datetime.ctime). |

## Signature

Represents a [pygit2 Signature](https://www.pygit2.org/objects.html#signatures).

Note: Some really old (decades old) Git Repos might have the Signature Object missing in some places. In that case the signature will just be `null`.

| Field Name | Type   | Description |
|------------|--------|-------------|
| `email`    | [Email](#Email) | The email address of the person as it is stored in the object. |
| `name`     | string | The name of the person. |
| `date`     | [Date](#Date) | The date and time associated with this signature. |
| `real.email`| [Email](#Email) | The real email address of the person according to the provided mailmap, or just the known email address if no entry was found in the mailmap. |
| `real.name`| string | The real name of the person according to the provided mailmap, or just the known name if no entry was found in the mailmap. |


## Object

The basic information about a Git Object.

| Field Name | Type   | Description |
|------------|--------|-------------|
| `id`       | [Oid](#Oid) | The Object ID. |
| `short_id` | string | An unambiguous abbreviation of id. |
| `type`     | string | The type of the Object. Either commit, tag, tree, or blob. |
| `type_num` | number | A numeric representation of `type`. This is for internal use only. Use `type` instead. |
| `raw`      | string | The [base64 encoding](https://docs.python.org/3/library/base64.html#base64.standard_b64encode) of the raw contents of the object. |

The following fields are only included if this Object is inside of [Repo Object](#Repo):

| Field Name | Type   | Description |
|------------|--------|-------------|
| `notes`    | \[[Note](#Note)\] | All Notes that are annotating this object. |
| `refs`    | \[[Reference](#Reference)\] | All References that are pointing at this object. |

## Commit

Represents a [pygit2 Commit](https://www.pygit2.org/objects.html#commits).

| Field Name | Type   | Description |
|------------|--------|-------------|
| `object`   | [Object](#Object) | Basic information about this object. |
| `author`   | [Signature](#Signature) | The author of the commit. |
| `committer`   | [Signature](#Signature) | The committer of the commit. |
| `date`     | [Date](#Date) | The commit date and time. |
| `message`  | \[string\] | The commit message split on newlines. |
| `raw_message` | string | The [base64 encoding](https://docs.python.org/3/library/base64.html#base64.standard_b64encode) of the raw commit message.|
| `message_encoding` | string | The encoding of the message. |
| `message_trailers` | string | See [here](https://www.pygit2.org/objects.html#pygit2.Commit.message_trailers) and [here](https://www.learnhowtoprogram.com/introduction-to-programming-part-time/git-html-and-css-part-2/commit-trailers-and-github-contributions#using-commit-trailers). |
| `parents` | \[[Oid](Oid)\] | The IDs of the parents of the commit. |
| `tree` | [Oid](#Oid) | The ID of the [Tree](#Tree) associated with this commit. |

## Tag

Represents a [pygit2 Tag](https://www.pygit2.org/objects.html#tags).

| Field Name | Type   | Description |
|------------|--------|-------------|
| `object`   | [Object](#Object) | Basic information about this object. |
| `tagger`   | [Signature](#Signature) | The Person who created this tag. |
| `name`     | string | The name of this tag. |
| `message`  | \[string\] | The tag message split on newlines. |
| `raw_message` | string | The [base64 encoding](https://docs.python.org/3/library/base64.html#base64.standard_b64encode) of the raw tag message.|
| `target`  | [Oid](#Oid) | The ID of the tagged Object. |

## Tree

Represents a [pygit2 Tree](https://www.pygit2.org/objects.html#trees).
This is basically a directory.

| Field Name | Type   | Description |
|------------|--------|-------------|
| `object`   | [Object](#Object) | Basic information about this object. |
| `items`   | \[[TreeItem](#TreeItem)\] | The items inside of the Tree. |

### TreeItem

A TreeItem basically represents a file or directory.

| Field Name | Type   | Description |
|------------|--------|-------------|
| `id`       | [Oid](#Oid) | The Object ID of the referenced Object. |
| `short_id` | string | An unambiguous abbreviation of id. |
| `type`     | string | The type of the referenced Object. Either  tree (= a Subdirectory), commit (= a Git Submodule (also a Subdirectory)), or blob (= a File). |
| `type_num` | number | A numeric representation of `type`. This is for internal use only. Use `type` instead. |
| `name`     | string | The name of the item. This is basically a filename. |
| `filemode` | [Filemode](#Filemode) |  The filemode. |

### Filemode

There are only six possible values for Filemode:

Directory:
```json
{
	"dec": 16384,
	"oct": "040000",
	"type": "dir",
	"is_gw": false,
	"is_exec": false
}
```

Regular non-executable file:
```json
{
	"dec": 33188,
	"oct": "100644",
	"type": "file",
	"is_gw": false,
	"is_exec": false
}
```

Regular non-executable group-writeable file:
```json
{
	"dec": 33204,
	"oct": "100664",
	"type": "file",
	"is_gw": true,
	"is_exec": false
}
```

Regular executable file:
```json
{
	"dec": 33261,
	"oct": "100755",
	"type": "file",
	"is_gw": false,
	"is_exec": true
}
```

Symbolic link:
```json
{
	"dec": 40960,
	"oct": "120000",
	"type": "symlink",
	"is_gw": false,
	"is_exec": false
}
```

Gitlink (Git Submodule):
```json
{
	"dec": 57344,
	"oct": "160000",
	"type": "gitlink",
	"is_gw": false,
	"is_exec": false
}
```

See also: [https://stackoverflow.com/questions/737673/how-to-read-the-mode-field-of-git-ls-trees-output#answer-8347325](https://stackoverflow.com/questions/737673/how-to-read-the-mode-field-of-git-ls-trees-output#answer-8347325)



## Blob

Represents a [pygit2 Blob](https://www.pygit2.org/objects.html#blobs).
This is basically the contents of a file.

The raw data of the blob can be found in `object.raw`.

| Field Name | Type   | Description |
|------------|--------|-------------|
| `object`   | [Object](#Object) | Basic information about this object. |
| `is_binary`| boolean | Whether the data is binary or not. |
| `size`     | number | The size of the blob in bytes. |
| `lines`    | \[string\] | The lines of the data interpreted as text. This field is omitted if `is_binary` is `false`. |


The following fields are only included if this Object is inside of [Repo Object](#Repo):

| Field Name | Type   | Description |
|------------|--------|-------------|
| `filenames` | Map(string, [Mimetype](#Mimetype)) | All known filenames for this Blob. |

### Mimetype

A Mimetype guessed based on the filename.

See also: [https://docs.python.org/3/library/mimetypes.html#mimetypes.guess_type](https://docs.python.org/3/library/mimetypes.html#mimetypes.guess_type)

| Field Name | Type   | Description |
|------------|--------|-------------|
| `type` | string | The type returend by `guess_type(filename, strict=True)`. |
| `encoding` | string | The encoding returend by `guess_type(filename, strict=True)`. |


## Reference

Represents a [pygit2 Reference](https://www.pygit2.org/references.html#pygit2.Reference).

Note: An annotated tag has the ID of a [Tag](#Tag) Object as a target.

| Field Name | Type   | Description |
|------------|--------|-------------|
| `name`     | string | The full name of the reference. Example: `refs/heads/main` |
| `type`     | string | Either `head` (a branch), `tag`, `remote`, or `note`.|
| `shorthand`| string | The shorthand "human-readable" name of the reference. |
| `is_symbolic`| boolean | Whether `target` is the referenced [Reference](#Reference)  or the referenced Object ID. |
| `target`   | [Reference](#Reference) \| [Oid](#Oid) | If `is_symbolic` is `true`, then this field is the referenced [Reference](#Reference), else this field is the Object ID of the referenced Object. |


## Note

Represents a [pygit2 Note](https://www.pygit2.org/references.html#the-note-type).

See also: https://git-scm.com/docs/git-notes

| Field Name | Type   | Description |
|------------|--------|-------------|
| `annotated_id` | [Oid](#Oid) | The ID of the annotated object. |
| `id`         | [Oid](#Oid) | The ID of this note. (This is also a commit generated by `git notes add`.) |
| `message`  | \[string\] | The note text split on newlines. |


## Repo

Represents an entire Git Repository with all of its data, files, commits, tags, references, notes, etc....

| Field Name | Type   | Description |
|------------|--------|-------------|
| `refs.heads`| Map(string, [Reference](#Reference)) | All head references. These are basically the branches of the repo. The Map key is the shortname of each reference. |
| `refs.tags`| Map(string, [Reference](#Reference)) | All tag references. The Map key is the shortname of each reference.|
| `refs.remotes`| Map(string, [Reference](#Reference)) | All remote references. The Map key is the shortname of each reference.|
| `refs.notes`| Map(string, [Reference](#Reference)) | All notes references. The Map key is the shortname of each reference. Note: The way git implements Notes is a bit weird. Use the `notes` Field instead. |
| `notes`    | \[[Note](#Note)\] | All the notes in the repository. |
| `objects`  | Map(string, Map(string, [Commit](#Commit) \| [Tag](#Tag) \| [Blob](#Blob) \| [Tree](#Tree) )) | All Objects referenced by their ID head ([Oid](#Oid).h) and tail ([Oid](#Oid).t). |
| `blobs`    | \[[Oid](#Oid)\] | IDs of all Blob Objects. |
| `commits`  | \[[Oid](#Oid)\] | IDs of all Commit Objects. |
| `trees`    | \[[Oid](#Oid)\] | IDs of all Tree Objects. |
| `tags`     | \[[Oid](#Oid)\] | IDs of all Tag Objects. |





