#!/usr/bin/env python3

from setuptools import setup

# see also https://betterscientificsoftware.github.io/python-for-hpc/tutorials/python-pypi-packaging/

setup(
    name='gr2j',
    version='0.1.3',
    description='GIT Repo to JSON --- Tool for converting an entire git repository with all commits, files, changes and everything else into one huge JSON file.',
    url='https://gitlab.gwdg.de/fissg/gr2j',
    author='Jake',
    author_email='j.vondoemming@stud.uni-goettingen.de',
    packages=['gr2j'],
    install_requires=['pygit2>=1.9.1',
                      ],
    entry_points={
        'console_scripts': [
            'gr2j=gr2j.__main__:main',
        ],
    },
    license="CC0-1.0",
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: CC0 1.0 Universal (CC0 1.0) Public Domain Dedication'
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Topic :: Software Development :: Version Control :: Git',
    ],
)


