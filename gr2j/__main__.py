#!/usr/bin/env python3

import gr2j

import argparse
import pygit2
import json
#import os
import sys
#import hashlib
#import base64
#import mimetypes
#from datetime import datetime, timezone, timedelta
#
## needed for RFC2822 dateformat
#from email import utils

#def print_object(o):
#    if (isinstance(o, pygit2.Reference)):
#      if (o.type == pygit2.GIT_REF_SYMBOLIC):
#          print_object(o.resolve())
#          return
#      print(type(o))
#      print(o)
#      print("name: " + o.name)
#      #print("raw_name: " + o.raw_name)
#      #print("raw_shorthand: " + o.raw_shorthand)
#      #print("raw_target: " + o.raw_target)
#      print("shorthand: " + o.shorthand)
#      print("target: " + oid_to_str(o.target))
#      print("type: " + str(o.type))
#      print("type(resolve()): " + str(type(o.resolve())))
#      print(o.resolve())
#      print('Target:')
#      target = repo.get(o.target)
#      #print(type(target))
#      print_object(target)
#    else:
#        object_json = json.dumps(object_to_dict(o), indent=4)
#        print(object_json)




def main():
    parser = argparse.ArgumentParser(prog="gr2j", description="GIT Repo to JSON --- Tool for converting an entire git repository with all commits, files, changes and everything else into one huge JSON file.")
    parser.add_argument("--version", help="print version and exit", action="store_true", dest="show_version")
    parser.add_argument("--debug", help="print debug output", action="store_true", dest="debug")
    parser.add_argument("-r", "--repo", help="path to the directory of the target repo (Example: '/opt/my_repo/.git' or '/opt/my_repo/') (Default: '.')", dest="repo", type=str, default=".")
    parser.add_argument("-i", "--indent", help="If INDENT is larger than 0, then the JSON will be prettyprinted using INDENT as level. 0  will only insert newlines. A negative value compacts the JSON as much as possible. (Default: -1)",
                    type=int, default=-1, dest="indent")
    parser.add_argument("-m", "--mailmap", help="Specify a mailmap file. See git-check-mailmap(1). Note: If this option isn't set, then an empty mailmap file will be assumed.", metavar="MAILMAPFILE", type=str, dest="mailmapfile")
    parser.add_argument("-d", "--dateformat", help="Specify a dateformat for the 'custom' field in a date object. (Default: '%%c %%z') See also: https://docs.python.org/3/library/datetime.html#strftime-and-strptime-format-codes", type=str, dest="dateformat", default="%c %z")
    parser.add_argument("-o", "--output", help="Write output to OUTPUTFILE instead of stdout. If OUTPUTFILE is -, output will go to stdout.", type=str, dest="outputfile", metavar="OUTPUTFILE")

    subparsers = parser.add_subparsers(title="commands", dest="command")

    parser_repo = subparsers.add_parser('repo', help='convert the entire repository to JSON')

    parser_obj = subparsers.add_parser('obj', help='convert an object of the repository to JSON')
    parser_obj.add_argument("OBJECTID", help="the hexdigest of the Object ID")


    parser_ref = subparsers.add_parser('ref', help='convert a reference of the repository to JSON')
    parser_ref.add_argument("REFERENCE", help="the reference")

    parser_email = subparsers.add_parser('email', help='convert an email address to JSON')
    parser_email.add_argument("ADDRESS", help="the email address")

    parser_date = subparsers.add_parser('date', help='convert a date to JSON')
    parser_date.add_argument("TIMESTAMP", help="the POSIX timestamp", type=int)
    parser_date.add_argument("--tzoffset", help="the timezone offset in minutes (Default: 0)", type=int, default=0)

    args = parser.parse_args()

    if args.debug:
        print(args, file=sys.stderr)

    if args.show_version:
        print("gr2j", gr2j.__version__)
        parser.exit(status=0)
    if not args.command:
        parser.print_help()
        parser.exit(status=1, message="\nError: missing command\n")


    # parse optional arguments
    repodir = args.repo
    indent = args.indent if args.indent >= 0 else None
    outfilename = args.outputfile if args.outputfile != "-" else None
    customdateformat = args.dateformat
    if args.mailmapfile:
        mailmap_file = open(args.mailmapfile, "r")
        mailmap = pygit2.Mailmap(mailmap_file.read())
        maimap_file.close()
    else:
        mailmap = pygit2.Mailmap()

    res_dict = None


    if args.command == "repo":
        repo = pygit2.Repository(repodir)
        res_dict = gr2j.repo_to_dict(repo, mailmap, customdateformat, repodir)
    elif args.command == "obj":
        repo = pygit2.Repository(repodir)
        obj = repo.get(args.OBJECTID)
        res_dict = gr2j.object_to_dict(obj, mailmap, customdateformat)
    elif args.command == "ref":
        repo = pygit2.Repository(repodir)
        ref = repo.lookup_reference_dwim(args.REFERENCE)
        res_dict = gr2j.reference_to_dict(ref, repo)
    elif args.command == "email":
        res_dict = gr2j.email_to_dict(args.ADDRESS)
    elif args.command == "date":
        res_dict = gr2j.date_to_dict(args.TIMESTAMP , args.tzoffset, customdateformat)
    else:
        parser.exit(status=1, message="Error: unknown command: " + args.command + "\n")

    res_json = json.dumps(res_dict, indent=indent, ensure_ascii=True)
    
    if outfilename:
        outfile = open(outfilename, "w")
        outfile.write(res_json)
        outfile.close()
    else:
        print(res_json)

    

if __name__ == '__main__':
    main()
