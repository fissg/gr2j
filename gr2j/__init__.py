#!/usr/bin/env python3

"""
GIT Repo to JSON

Tool for converting an entire git repository with all commits, files, changes and everything else into one huge JSON file.
"""

from gr2j.gr2j import *

__author__ = 'Jake'
__version__ = '0.1.3'
__license__ = 'CC0-1.0'

