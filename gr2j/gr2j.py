#!/usr/bin/env python3

#import argparse
import pygit2
#import json
import os
import sys
import hashlib
import base64
import mimetypes
from datetime import datetime, timezone, timedelta

from email import utils # needed for RFC2822 dateformat

def bytes_to_b64(b):
    return base64.standard_b64encode(b).decode('utf-8')
    #return b.decode('utf-8', "replace")
    #return b.decode('utf-8', "backslashreplace")

def oid_to_str(id):
    if (isinstance(id, pygit2.Oid)):
        return id.hex
    else:
        return id

def oid_to_dict(id):
    res = {}
    s = oid_to_str(id)
    res["full"] = s
    res["abbrev"] = s[0:8]
    res["h"] = s[0:2] # head
    res["t"] = s[2:] # tail
    return res

def email_to_dict(email):
    res = {}
    res["orig"] = email
    le = email.lower() # lowercase is more useful in most cases
    le = le.lstrip().rstrip() # remove leading and trailing whitespace
    res["lower"] = le
    s = le.split("@") # TODO handle "illegal" email addresses (no or multiple @ signs)
    res["local_part"] = s[0]
    res["domain"] = s[1]
    res["md5"] = hashlib.md5(le.encode('utf-8')).hexdigest() # can be used for Gravatar
    return res

def date_to_dict(time,offset, customdateformat):
    # see https://docs.python.org/3/library/datetime.html
    res = {}
    tzinfo = timezone(timedelta(minutes=offset))
    dt = datetime.fromtimestamp(float(time), tzinfo)
    res["custom"] = dt.strftime(customdateformat)
    res["rfc2822"] = utils.format_datetime(dt)
    res["year"] = dt.year
    res["month"] = dt.month
    res["day"] = dt.day
    res["hour"] = dt.hour
    res["minute"] = dt.minute
    res["second"] = dt.second
    res["microsecond"] = dt.microsecond
    #res["tzinfo"] = dt.tzinfo
    #res["utcoffset"] = dt.utcoffset()
    #res["dst"] = dt.dst() # TODO maybe add dst back
    res["tzoffset_in_minutes"] = offset
    res["tzname"] = dt.tzname()
    #res["posix_timestamp"] = dt.timestamp() # TO_DO: catch potential error
    res["posix_timestamp"] = time
    res["weekday"] = dt.weekday() # 0: Monday 6: Sunday
    res["isoweekday"] = dt.isoweekday() # 1: Monday 7: Sunday
    res["isocalendar"] = dt.isocalendar()
    res["iso8601"] = dt.isoformat()
    res["ctime"] = dt.ctime()
    # TODO add fold https://docs.python.org/3/library/datetime.html#datetime.datetime.fold
    return res

def signature_to_dict(s, mailmap, customdateformat):
    if not s: # Some really old git repos might miss a signature on some places (like tags for example 'v0.99' in the git source code repo).
        return None
    res = {}
    res["email"] = email_to_dict(s.email)
    res["name"] = s.name
    res["date"] = date_to_dict(s.time,s.offset,customdateformat)
    resmm = {}
    (resmm["name"], resmmemail) = mailmap.resolve(s.name, s.email)
    resmm["email"] = email_to_dict(resmmemail)
    #res["respecting_mailmap"] = resmm
    res["real"] = resmm
    return res



def internal_object_to_dict(o, include_raw = True):
    res = {}
    #res["hex"] = o.hex
    res["id"] = oid_to_dict(o.id)
    #res["oid"] = oid_to_dict(o.oid)
    res["short_id"] = o.short_id # short_id is unambiguous, while id.abbrev is ambiguous!
    res["type_num"] = o.type
    res["type"] = o.type_str
    if include_raw:
        res["raw"] = bytes_to_b64(o.read_raw())
    return res

def commit_to_dict(c, mailmap, customdateformat):
    res = {}
    res["object"] = internal_object_to_dict(c)
    res["author"] = signature_to_dict(c.author, mailmap, customdateformat)
    res["date"] = date_to_dict(c.commit_time,c.commit_time_offset, customdateformat)
    res["committer"] = signature_to_dict(c.committer, mailmap, customdateformat)
    # TODO gpg_signature
    #res["message"] = c.message
    res["message"] = c.message.splitlines()
    res["raw_message"] = bytes_to_b64(c.raw_message)
    res["message_encoding"] = c.message_encoding
    res["message_trailers"] = c.message_trailers
    parents = []
    for p in c.parent_ids:
        parents.append(oid_to_dict(p))
    res["parents"] = parents
    res["tree"] = oid_to_dict(c.tree_id)
    # TODO diff & patch zu parents
    # TODO add describe (see https://www.pygit2.org/repository.html?highlight=merged#pygit2.Repository.describe)
    return res

def tag_to_dict(t, mailmap, customdateformat):
    res = {}
    #print(oid_to_str(t.id))
    res["object"] = internal_object_to_dict(t)
    res["tagger"] = signature_to_dict(t.tagger, mailmap, customdateformat)
    res["name"] = t.name
    #res["message"] = t.message
    res["message"] = t.message.splitlines()
    res["raw_message"] = bytes_to_b64(t.raw_message)
    res["target"] = oid_to_dict(t.target)
    return res

def tree_to_dict(t):
    res = {}
    res["object"] = internal_object_to_dict(t)
    #print("tree: " + oid_to_str(t.id))
    items = []
    for o in t:
        # TODO handle git submodules
        if o.type == pygit2.GIT_OBJ_COMMIT: # git submodule should go here
            print("Warn: Not handling git submodule in tree(" + oid_to_str(t.short_id) + "): " + oid_to_str(o.id) + " " + o.type_str + " " + o.name, file = sys.stderr)
            #raise Exception("Error: Not handling git submodule in tree(" + oid_to_str(t.short_id) + "): " + oid_to_str(o.id) + " " + o.type_str + " " + o.name, file = sys.stderr)
            od = {}
        else:
            od = internal_object_to_dict(o, include_raw=False)
        od["name"] = o.name
        switch={ # see https://stackoverflow.com/questions/737673/how-to-read-the-mode-field-of-git-ls-trees-output#answer-8347325
                16384 : { # 0100000000000000 (040000): Directory
                    "oct": "040000",
                    "type": "dir",
                    "is_gw": False,
                    "is_exec": False
                },
                33188 : { # 1000000110100100 (100644): Regular non-executable file
                    "oct": "100644",
                    "type": "file",
                    "is_gw": False,
                    "is_exec": False
                },
                33204 : { # 1000000110110100 (100664): Regular non-executable group-writeable file
                    "oct": "100664",
                    "type": "file",
                    "is_gw": True,
                    "is_exec": False
                },
                33261 : { # 1000000111101101 (100755): Regular executable file
                    "oct": "100755",
                    "type": "file",
                    "is_gw": False,
                    "is_exec": True
                },
                40960 : { # 1010000000000000 (120000): Symbolic link
                    "oct": "120000",
                    "type": "symlink",
                    "is_gw": False,
                    "is_exec": False
                },
                57344 : { # 1110000000000000 (160000): Gitlink (Git Submodule)
                    "oct": "160000",
                    "type": "gitlink",
                    "is_gw": False,
                    "is_exec": False
                }
            }
        fm = switch.get(o.filemode, None)
        fm["dec"] = o.filemode
        #if not fm: # commented out for better performance
        #    raise Exception("unknown filemode" + " " + str(o.filemode) + " " + oid_to_str(o.id))
        od["filemode"] = fm
        # TODO add file history
        items.append(od)
    res["items"] = items
    return res

def blob_to_dict(b):
    res = {}
    # data is found in object.raw
    res["object"] = internal_object_to_dict(b)
    res["is_binary"] = b.is_binary
    res["size"] = b.size
    if not b.is_binary:
        data_string = b.data.decode('utf-8', "replace")
        res["lines"] = data_string.splitlines()
        #try:
        #    json_data = json.loads(data_string)
        #    res["json_data"] = json_data
        #except ValueError as e:
        #    res["json_data"] = None
    # TODO hashes
    return res

def object_to_dict(o, mailmap, customdateformat):
    if (isinstance(o, pygit2.Commit)):
        return commit_to_dict(o, mailmap, customdateformat)
    elif (isinstance(o, pygit2.Tag)):
        return tag_to_dict(o, mailmap, customdateformat)
    elif (isinstance(o, pygit2.Tree)):
        return tree_to_dict(o)
    elif (isinstance(o, pygit2.Blob)):
        return blob_to_dict(o)
    else:
        #raise Exception("Unknown object type: " + str(type(o)))
        print("Unknown object type: " + str(type(o)))


def reference_to_dict(ref_str, repo):
    res = {}
    if (isinstance(ref_str, pygit2.Reference)):
        r = ref_str
    else:
        r = repo.lookup_reference(ref_str)
    res["name"] = r.name
    res["type"] = r.name.split("/")[1][:-1]
    #res["raw_name"] = bytes_to_b64(r.raw_name)
    res["shorthand"] = r.shorthand
    #res["raw_shorthand"] = bytes_to_b64(r.raw_shorthand)
    if r.type == pygit2.GIT_REF_OID:
        res["is_symbolic"] = False
        res["target"] = oid_to_dict(r.target)
    elif r.type == pygit2.GIT_REF_SYMBOLIC:
        res["is_symbolic"] = True
        res["target"] = reference_to_dict(r.target, repo)
    else:
        raise Exception("Unknown reference type of " + r.name + ": " + str(r.type))
    
    return res


def register_all_oids(reg, repo_dir, use_git_cat_file_cmd = True):
    if use_git_cat_file_cmd:
        # This is faster, but has some problems:
        # - invokes the git command
        # - other than the other method, this doesn't register submodule commit ids inside of trees
        # - ...
        cwd = os.getcwd()
        os.chdir(repo_dir) # Update current working dir
        stream = os.popen("git cat-file --batch-check --batch-all-objects", 'r') # This uses the current working directory as git repo
        os.chdir(cwd)
        #print(type(stream))
        while True:
            line = stream.readline()
            if not line:
                break
            line = line.strip() # remove newline
            s = line.split(" ")
            oid_str = s[0]
            oid = pygit2.Oid(hex=oid_str)
            #object_type = s[1]
            #size = s[2]
            #print("line: '" + line + "'")
            #print(s)
            reg.append(oid)
        stream.close()
    else:
        # Warn: this will break if submodules occure
        stack = []
        for ref_str in repo.references:
            #print("ref_str: " + ref_str)
            ref = repo.lookup_reference(ref_str)
            if (ref.type == pygit2.GIT_REF_SYMBOLIC):
                ref = ref.resolve()
            #print(ref.target)
            if isinstance(repo.get(ref.target), pygit2.Commit):
                for commit in repo.walk(ref.target):
                    #register_all_oids_by_object(reg, commit)
                    stack.append(commit)
            #register_all_oids_by_object(reg, ref.target)
            stack.append(ref.target)
        while len(stack) > 0:
            o = stack.pop()
            register_all_oids_by_object(reg, stack, o)
            #print(len(reg))
            #print(stack)

def register_all_oids_by_object(reg, stack, o):
    if isinstance(o, pygit2.Oid):
        o = repo.get(o)
    if not isinstance(o, pygit2.Object):
        print(type(o))
        print(o)
        raise Exception("o is not an object: " + str(type(o)))
    if o.id not in reg:
        reg.append(o.id)
        #print(len(reg))
        if isinstance(o, pygit2.Commit):
            for parent_id in o.parent_ids:
                parent = repo.get(parent_id)
                #register_all_oids_by_object(reg, parent)
                stack.append(parent)
            #register_all_oids_by_object(reg, o.tree)
            stack.append(o.tree)
        elif isinstance(o, pygit2.Tree):
            for t in o:
                #print(t)
                #register_all_oids_by_object(reg, t)
                stack.append(t)
        elif isinstance(o, pygit2.Tag):
            #register_all_oids_by_object(reg, o.target)
            stack.append(o.target)

def note_to_dict(n):
    res = {}
    res["annotated_id"] = oid_to_dict(n.annotated_id)
    res["id"] = oid_to_dict(n.id)
    res["message"] = n.message.splitlines()
    return res
    


def repo_to_dict(repo, mailmap, dateformat, repo_dir):
    # TODO caching

    res = {}

    oidregister = []
    register_all_oids(oidregister, repo_dir)

    # add objects
    objs = {}
    blobs = []
    commits = []
    trees = []
    tags = []
    for oid in oidregister:
        o = repo.get(oid)
        od = object_to_dict(o, mailmap, dateformat)
        obj = od["object"]
        obj["notes"] = [] # add empty array for potential notes
        obj["refs"] = [] # add empty array for potential later backreferencing
        ioid = obj["id"]
        objs.setdefault(ioid["h"], {})[ioid["t"]] = od
        if obj["type_num"] == pygit2.GIT_OBJ_COMMIT:
            commits.append(ioid)
        elif obj["type_num"] == pygit2.GIT_OBJ_BLOB:
            blobs.append(ioid)
        elif obj["type_num"] == pygit2.GIT_OBJ_TREE:
            trees.append(ioid)
        elif obj["type_num"] == pygit2.GIT_OBJ_TAG:
            tags.append(ioid)


    # add references
    refs = {}
    refs["heads"] = {}
    refs["tags"] = {}
    refs["remotes"] = {}
    refs["notes"] = {}
    for ref_str in repo.references:
        ref = reference_to_dict(ref_str, repo)
        if ref["type"] == "head":
            refs["heads"][ref["shorthand"]] = ref
        elif ref["type"] == "tag":
            refs["tags"][ref["shorthand"]] = ref
        elif ref["type"] == "remote":
            refs["remotes"][ref["shorthand"]] = ref
        elif ref["type"] == "note":
            refs["notes"][ref["shorthand"]] = ref
        else:
            raise Exception("Unknown reference type '" + str(ref["type"]) + "' for ref: " + str(ref_str))
        # add backreference to object
        target = ref["target"]
        if ref["is_symbolic"]:
            target = target["target"]
        objs[target["h"]][target["t"]]["object"]["refs"].append(ref)
    res["refs"] = refs

    # TODO submodules

    mimetypes.init()

    # add known filenames & mimetypes to blobs
    for t_id in trees:
        t = objs[t_id["h"]][t_id["t"]]
        for f in t["items"]:
            if "id" not in f:
                continue # ignore submodule
            f_id = f["id"]
            blob = objs[f_id["h"]][f_id["t"]] 
            f_name = f["name"]
            if f_name not in blob.setdefault("filenames", {}):
                guess = mimetypes.guess_type(f_name, strict=True) # see https://docs.python.org/3/library/mimetypes.html#mimetypes.guess_type
                blob["filenames"][f_name] = {
                        "type": guess[0],
                        "encoding": guess[1],
                }
                #print(json.dumps(objs[f_id["h"]][f_id["t"]]))


    # add notes to objects
    notes = []
    try:
        for n in repo.notes():
            nd = note_to_dict(n)
            notes.append(nd)
            o_id = nd["annotated_id"]
            o = objs[o_id["h"]][o_id["t"]]
            o["object"]["notes"].append(nd)
    except:
        pass
    #raise Exception("Debug")
    res["notes"] = notes


    res["objects"] = objs
    res["blobs"] = blobs
    res["commits"] = commits
    res["trees"] = trees
    res["tags"] = tags
    return res

