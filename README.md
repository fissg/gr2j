# GIT Repo to JSON

Tool for converting an entire git repo with all commits, files, changes and everything else into one huge json file.

# Dependencies

- Python3 (>=3.6)
- python3-pip (Tested with version 20.0.2)
- pygit2 (`pip3 install pygit2`)
- argparse (`pip3 install argparse`)
- Git (Tested with version 2.36.0.)

## Install

1. Clone  this repository: `git clone https://gitlab.gwdg.de/fissg/gr2j.git gr2j`
2. `cd gr2j`
3. Install: `pip install .`

## Usage

```
usage: gr2j [-h] [--version] [--debug] [-i INDENT] [-m MAILMAPFILE] [-d DATEFORMAT] [-o OUTPUTFILE] {repo,obj,ref,email,date} ...

GIT Repo to JSON --- Tool for converting an entire git repository with all commits, files, changes and everything else into one huge JSON
file.

optional arguments:
  -h, --help            show this help message and exit
  --version             print version and exit
  --debug               print debug output
  -i INDENT, --indent INDENT
                        If INDENT is larger than 0, then the JSON will be prettyprinted using INDENT as level. 0 will only insert
                        newlines. A negative value compacts the JSON as much as possible. (Default: -1)
  -m MAILMAPFILE, --mailmap MAILMAPFILE
                        Specify a mailmap file. See git-check-mailmap(1). Note: If this option isn't set, then an empty mailmap file will
                        be assumed.
  -d DATEFORMAT, --dateformat DATEFORMAT
                        Specify a dateformat for the 'custom' field in a date object. (Default: '%c %z') See also:
                        https://docs.python.org/3/library/datetime.html#strftime-and-strptime-format-codes
  -o OUTPUTFILE, --output OUTPUTFILE
                        Write output to OUTPUTFILE instead of stdout. If OUTPUTFILE is -, output will go to stdout.

commands:
  {repo,obj,ref,email,date}
    repo                convert the entire repository to JSON
    obj                 convert an object of the repository to JSON
    ref                 convert a reference of the repository to JSON
    email               convert an email address to JSON
    date                convert a date to JSON
```

## Interpreting the JSON output

See [SCHEMA.md](SCHEMA.md).

## Note on using notes

Push all notes:
```bash
git push origin refs/notes/*
```

Fetch all notes:
```bash
git fetch origin refs/notes/*:refs/notes/*
```

https://stackoverflow.com/questions/18268986/git-how-to-push-messages-added-by-git-notes-to-the-central-git-server

## TODOs for v1.0.0


- [ ] further improvements to the date JSON representation
- [ ] interpret gpg signatures in commits
- [ ] add diffs & patches in commits
- [ ] add file history in tree
- [ ] add hashes to BLOBs
- [ ] add submodule support
- [ ] add caching support (nice to have)


## Links

- https://git-scm.com/docs/gitrepository-layout
- https://git-scm.com/docs/git-show-ref
- https://git-scm.com/docs/git-for-each-ref
- https://git-scm.com/docs/git-describe
- https://git-scm.com/docs/git-hash-object
- https://git-scm.com/docs/git-show
- https://git-scm.com/docs/git-cat-file
- https://git-scm.com/docs/diff-format
- https://git-scm.com/book/en/v2/Git-Internals-Git-Objects


## Useful Commands

- `cat 8a474ba2c3ff36ad1d6176d740b4c69f7df7b6 | zlib-flate -uncompress` (zlib-flate provided by the `qpdf` package)

## How to version bump

1. Test if everything works!
1. Update `version` in `./setup.py`.
1. Update `__version__` in `./gr2j/__init__.py`.
1. Commit and Push (message: "version bump")
1. List commits since last Tag.
1. Create new Tag on GitLab:
	- Tag name: "vX.Y.Z"
	- Message: "Release vX.Y.Z"
	- Release notes: "Release vX.Y.Z\n [Changelog]"


